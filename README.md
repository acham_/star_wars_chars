# Star Wars Character Query Tool
Retrieve various information on Star Wars characters

usage: 
`get_character.py [options] character_name`

positional arguments:

- `character_name`  the name of the character of interest

optional arguments:

  - -h, --help      show this help message and exit
  - -f, --films     display the names of films this character appears in
  - -g, --gender    display the character's gender
  - -H, --home      display the character's home world
  - -m, --mass      display the character's mass
  - -s, --species   display the character's species

## Notes
The app uses the API's search functionality to search for 
the character name provided by the user. However, the
name provided must exactly match a character name in the API
for the app to return character information. If it doesn't, a list
of valid character names is returned.

Also, the API returns an empty list of species URLs when the character is 
human, so the app sets the species to "Human" in such cases.
