import argparse
import json
import requests
import sys


BASE_URL = "https://swapi.dev/api"


def parse_args():
    parser = argparse.ArgumentParser(
        usage='%(prog)s [options] character_name',
        description='Retrieve various information on one Star Wars character')
    parser.add_argument('character_name',
                        nargs='+',
                        metavar='character_name',
                        type=str,
                        help='the name of the character of interest')

    parser.add_argument('-f', '--films', dest='films', action='store_true',
                        help='display the names of films this character appears in')
    parser.add_argument('-g', '--gender', dest='gender', action='store_true',
                        help="display the character's gender")
    parser.add_argument('-H', '--home', dest='home', action='store_true',
                        help="display the character's home world")
    parser.add_argument('-m', '--mass', dest='mass', action='store_true',
                        help="display the character's mass")
    parser.add_argument('-s', '--species', dest='species', action='store_true',
                        help="display the character's species")
    args = parser.parse_args()

    return args


def get_character_names():
    """Retrieves a list of available character names"""

    response = requests.get(f"{BASE_URL}/people")
    names = []
    for person in response.json()['results']:
        names.append(person['name'])

    return names


def get_film_titles_from_urls(film_urls):
    """Given a list of URLs for films, returns a list of the corresponding film titles"""

    film_titles = []
    for film_url in film_urls:
        response = requests.get(film_url)
        film_title = response.json().get('title')
        if film_title is not None:
            film_titles.append(film_title)

    return film_titles


def get_species_names_from_urls(species_urls):
    """Given a list of URLs for species, returns a list of the corresponding species names."""

    species_names = []

    if len(species_urls) == 0:
        return ['Human']

    for species_url in species_urls:
        response = requests.get(species_url)
        species_name = response.json().get('name')
        if species_name is not None:
            species_names.append(species_name)

    return species_names


def get_planet_name(planet_url):
    """Gets a planet name given its URL."""

    response = requests.get(planet_url)
    planet_name = response.json().get('name')
    if planet_name is None:
        print(f"Encountered invalid URL: {planet_url}")

    return planet_name


def check_main_response(character_response, character_name):
    """Error checking on main response data"""

    results = character_response.get('results')
    if results is None \
        or len(results) != 1 \
        or results[0]['name'] != character_name:
        print(f"Could not find information for character: {character_name}")
        print(f"Available character names: {get_character_names()}")
        sys.exit(1)

    return


if __name__ == '__main__':
    args = parse_args()

    # In case character name has multiple parts
    character_name = ' '.join(args.character_name)
    print(f'Searching for information on character: {character_name}')

    # Encode the character name for a query string
    character_name_encoded = requests.utils.quote(character_name)
    r = requests.get(f'{BASE_URL}/people/?search={character_name_encoded}')

    # Error checking on main response
    character_response = r.json()
    check_main_response(character_response, character_name)
    character_results = character_response.get('results')[0]

    # Build output dictionary with character info
    character_info = {}
    if args.films:
        character_info['films'] = get_film_titles_from_urls(character_results.get('films'))
    if args.gender:
        character_info['gender'] = character_results.get('gender')
    if args.home:
        character_info['homeworld'] = get_planet_name(character_results.get('homeworld'))
    if args.mass:
        character_info['mass'] = character_results.get('mass')
    if args.species:
        character_info['species'] = get_species_names_from_urls(character_results.get('species'))

    print(json.dumps(character_info, sort_keys=True, indent=4))
